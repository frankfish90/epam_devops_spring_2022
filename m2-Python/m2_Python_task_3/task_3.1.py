import random
import string

# Generate password
# Write a utility for generating passwords according to a given template that supports the CLI interface and logging

# x = 'A4%d3%-%a2%'
a = input("Enter template for password A4%d3%-%a2%: ")

listIt = a.split('%')
listIt = listIt[0:-1]
print(listIt)

password_list2 = []
startIndex = 0
for item in listIt:
    if "a" in item:
        for startIndex in range(int(item[1:])):
            password_list2 += random.choices(string.ascii_lowercase)
    elif "A" in item:
        for startIndex in range(int(item[1:])):
            password_list2 += random.choices(string.ascii_uppercase)
    elif "d" in item:
        for startIndex in range(int(item[1:])):
            password_list2 += random.choices(string.digits)
    elif "p" in item:
        password_list2 += random.choices(string.punctuation)
    elif "-" in item:
        password_list2 += "-"
    elif "@" in item:
        password_list2 += "@"

password2 = "".join(password_list2)
print(password2)
