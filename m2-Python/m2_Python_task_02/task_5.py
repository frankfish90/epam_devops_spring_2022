print('Задание_5')
# Из строк command1 и command2 получить список VLAN-ов, которые есть и в команде command1 и в команде command2.
# Результатомдолженбытьсписок:['1', '3', '8']
command1 = 'switchport trunk allowed vlan 1,2,3,5,8'
command2 = 'switchport trunk allowed vlan 1,3,8,9'

def task3(command):
    newLine = command[command.find('1')::]
    print(newLine)
    array = newLine.split(',')
    print(array)
    return array


com1 = task3(command1)
com2 = task3(command2)

c1 = set(com1)
c2 = set(com2)
c = c1.intersection(c2)
print(c)