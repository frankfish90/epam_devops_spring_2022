# Преобразовать IP-адрес в двоичный формат и вывести на стандартный поток вывода вывод столбцами, таким образом:

ip = '192.168.3.1'
ipaddress: list[int] = ip.split('.')
print(ipaddress)

print(f'''
...: IP address:
...: {int(ipaddress[0]):<8} {int(ipaddress[1]):<8} {int(ipaddress[2]):<8} {int(ipaddress[3]):<8}
...: {int(ipaddress[0]):08b} {int(ipaddress[1]):08b} {int(ipaddress[2]):08b} {int(ipaddress[3]):08b}
''')
