print("Task 6")
ospf_route = 'OSPF 10.0.24.0/24 [110/41] via 10.0.13.3, 3d18h, FastEthernet0/0'
list_task_6 = ospf_route.split(' ')
index = list_task_6.index('via')
list_task_6.pop(index)
print(list_task_6)
print(f'Protocol: {list_task_6[0]}\nPrefix: {list_task_6[1]}\nAD/Metric: {list_task_6[2]}\nNext-Hop: {list_task_6[3]}\nLast update: {list_task_6[4]}\nOutbound Interface: {list_task_6[5]}')
