print("Task 6")
ospf_route = 'OSPF 10.0.24.0/24 [110/41] via 10.0.13.3, 3d18h, FastEthernet0/0'
ospf_route = ospf_route.split(',')
print(ospf_route)
ospf_route1 = ospf_route[0].split()
print(ospf_route1)
ospf_route = ospf_route1 + ospf_route[-2:]
print(ospf_route)
print(f'Protocol: {ospf_route[0]}\nPrefix: {ospf_route[1]}\nAD/Metric: {ospf_route[2]}\nNext-Hop: {ospf_route[4]}\nLast update: {ospf_route[5]}\nOutbound Interface: {ospf_route[6]}')