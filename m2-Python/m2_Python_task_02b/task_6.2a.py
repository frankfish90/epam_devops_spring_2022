#Задание 6.2a
#Сделать копию скрипта задания 6.2.
#Добавить проверку введенного IP-адреса. Адрес считается корректно заданным, если он:
#• состоит из 4 чисел разделенных точкой,
#• каждое число в диапазоне от 0 до 255.
#Если адрес задан неправильно, выводить сообщение: „Неправильный IP-адрес“

while True:
    a = input("IP-адрес in format x.x.x.x: ")
    byte = a.split('.')
    first_byte = int(byte[0])
    sec_byte = int(byte[1])
    third_byte = int(byte[2])
    four_byte = int(byte[3])
    if first_byte < 256 and sec_byte < 256 and third_byte < 256 and four_byte < 256:
        print("Ip-address: " + a + " is not correct")
        break
    elif 0 < first_byte < 224:
        print("Ip-address: " + a + " unicast")
        break
    elif 223 < first_byte < 240:
        print("Ip-address: " + a + " multicast")
        break
    elif a == '255.255.255.255':
        print("Ip-address: " + a + " local broadcast")
        break
    elif a == '0.0.0.0':
        print("Ip-address: " + a + " unassigned")
        break
    else:
        print("Ip-address: " + a + " unused")
        break
