# Запросить у пользователя ввод IP-адреса в формате 10.0.1.1
# Определить тип IP-адреса.
# В зависимости от типа адреса, вывести на стандартный поток вывода:
# „unicast“ - если первый байт в диапазоне 1-223
# „multicast“ - если первый байт в диапазоне 224-239
# „local broadcast“ - если IP-адрес равен 255.255.255.255
# „unassigned“ - если IP-адрес равен 0.0.0.0
# „unused“ - во всех остальных случаях


while True:
    a = input("IP-адрес in format x.x.x.x : ")
    byte = a.split('.')
    first_byte = int(byte[0])
    if 1 < first_byte < 224:
        print("unicast")
        break
    elif 224 < first_byte < 239:
        print("multicast")
        break
    elif a == '255.255.255.255':
        print("local broadcast")
        break
    elif a == '0.0.0.0':
        print("unassigned")
        break
    else:
        print("unused")
        break
